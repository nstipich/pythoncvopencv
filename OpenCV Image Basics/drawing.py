import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

blankImage = np.zeros(shape=(512, 512, 3), dtype=np.int16)
plt.imshow(blankImage)

cv2.rectangle(blankImage, pt1=(256, 0),
              pt2=(512, 256), color=(0, 255, 0),
              thickness=10)
cv2.circle(blankImage, (256, 256), 50, (255, 0, 0), -1)     # -1 to fill
cv2.line(blankImage, (256, 256), (512, 0), (0, 0, 255), 5)

font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(blankImage, 'Tutorial', (0, 256),
            font, 1, (255, 255, 0), 3, cv2.LINE_AA)

vertices = np.array([[100, 300], [200, 200],
                    [400, 300], [200, 400]],
                    np.int32)
vertices.shape
points = vertices.reshape((-1, 1, 2))   # [[[]]]
points.shape

cv2.polylines(blankImage, [points], True, (255, 255, 255))
plt.imshow(blankImage)
