import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath
import math

PATH = includePath()

drawing = False
ix, iy = -1, -1


def draw_circle(event, x, y, flags, param):  # onMouse function
    if event == cv2.EVENT_LBUTTONDOWN:
        if flags == cv2.EVENT_LBUTTONDOWN + cv2.EVENT_FLAG_ALTKEY:
            cv2.circle(image, (x, y), 10, (0, 255, 0), 1)
        elif flags == cv2.EVENT_LBUTTONDOWN + cv2.EVENT_FLAG_CTRLKEY:
            cv2.circle(image, (x, y), 10, (0, 255, 255), 1)
        else:
            cv2.circle(image, (x, y), 10, (255, 255, 255), 1)
    elif event == cv2.EVENT_RBUTTONDOWN:
        cv2.circle(image, (x, y), 10, (255, 0, 0), 1)


def draw_rectangle(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        global ix, iy, drawing
        if event == cv2.EVENT_LBUTTONDOWN:
            drawing = True
            ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing:
            cv2.rectangle(image, (ix, iy),
                          (x, y), (0, 0, 255), -1)
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        cv2.rectangle(image, (ix, iy), (x, y),
                      (0, 0, 255), -1)



cv2.namedWindow('Image')
cv2.setMouseCallback('Image', draw_rectangle)  # function is being passed on, no ()


image = np.zeros((512, 512, 3))


while True:
    cv2.imshow('Image', image)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break
cv2.destroyAllWindows()
