import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

picture = cv2.imread(PATH + '/DATA/00-puppy.jpg')
grayPicture = cv2.imread(PATH + '/DATA/00-puppy.jpg', cv2.IMREAD_GRAYSCALE)
pltPicture = cv2.cvtColor(picture, cv2.COLOR_BGR2RGB)

squashed = cv2.resize(picture, (640, 480))
wRatio = 0.5
hRatio = 0.5
squashed2 = cv2.resize(picture, (0, 0), picture, wRatio, hRatio)
flippedPicture = cv2.flip(picture, -1)

cv2.imwrite(PATH + '/DATA/squashedPuppy.jpg', squashed)

cv2.imshow('Puppy', picture)
# cv2.waitKey()
cv2.imshow('Gray Puppy', grayPicture)
cv2.imshow('Resized puppy', squashed)
cv2.imshow('Halved puppy', squashed2)
cv2.imshow('Flipped puppy', flippedPicture)
plt.imshow(pltPicture)
plt.imshow(grayPicture, cmap='gray')
