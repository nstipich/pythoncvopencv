import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

flat_chess = cv2.imread(PATH + '/DATA/flat_chessboard.png')
flat_chess = cv2.cvtColor(flat_chess, cv2.COLOR_BGR2RGB)
gray_flat_chess = cv2.imread(PATH + '/DATA/flat_chessboard.png', 0)
real_chess = cv2.imread(PATH + '/DATA/real_chessboard.jpg')
real_chess = cv2.cvtColor(real_chess, cv2.COLOR_BGR2RGB)
gray_real_chess = cv2.imread(PATH + '/DATA/real_chessboard.jpg', 0)

corners = cv2.goodFeaturesToTrack(gray_flat_chess, 32, 0.01, 10)
corners = np.int0(corners)
for corner in corners:
    x, y = corner.ravel()   # ravel flattens the array
    cv2.circle(flat_chess, (x, y), 3, (255, 0, 0), -1)
plt.imshow(flat_chess)

corners = cv2.goodFeaturesToTrack(gray_real_chess, 140, 0.01, 10)
corners = np.int0(corners)
for corner in corners:
    x, y = corner.ravel()   # ravel flattens the array
    cv2.circle(real_chess, (x, y), 3, (255, 0, 0), -1)
plt.imshow(real_chess)
