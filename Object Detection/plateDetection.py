import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath


def plate_detect(image):
    image_copy = image.copy()
    plate_image = plate_cascade.detectMultiScale(image_copy)
    for (x, y, w, h) in plate_image:
        #cv2.rectangle(image_copy, (x, y), (x + w, y + h), (255, 255, 255), 10)
        image_copy[y:y+h, x:x+w] = cv2.medianBlur(image_copy[y:y+h, x:x+w], 75)
    return image_copy


PATH = includePath()
car = cv2.imread(PATH + '/DATA/car_plate.jpg', 0)

plate_cascade = cv2.CascadeClassifier(PATH + '/DATA/haarcascades/haarcascade_licence_plate_rus_16stages.xml')
blurred_plate = plate_detect(car)
cv2.imshow('Loaded image', car)
cv2.imshow('Blurred plate', blurred_plate)

