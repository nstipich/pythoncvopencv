import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

full = cv2.imread(PATH + '/DATA/sammy.jpg')
full = cv2.cvtColor(full, cv2.COLOR_BGR2RGB)
face = cv2.imread(PATH + '/DATA/sammy_face.jpg')
face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)

# eval function f.e. func = eval('sum')

methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR', 'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']

figure = 1

for m in methods:
    full_copy = full.copy()
    method = eval(m)

    result = cv2.matchTemplate(full_copy, face, method)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:    # square diff methods use min value
        top_left = min_loc  # (x,y)
    else:
        top_left = max_loc
    height, width, channel = face.shape
    bot_right = (top_left[0] + width, top_left[1]+height)
    cv2.rectangle(full_copy, top_left, bot_right, (255, 0, 0), 10)
    plt.figure(figure)
    plt.subplot(121)
    plt.imshow(result)
    plt.title('Template matching heatmap')
    plt.subplot(122)
    plt.imshow(full_copy)
    plt.title('Detected match')
    plt.suptitle(m)
    plt.show()
    figure = figure + 1
