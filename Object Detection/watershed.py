import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

sep_coins = cv2.imread(PATH + '/DATA/pennies.jpg')
plt.imshow(sep_coins)

# Manual Watershed Algorithm #

# Median Blur
sep_blur = cv2.medianBlur(sep_coins, 25)
# Grayscale
gray_sep_coins = cv2.cvtColor(sep_blur, cv2.COLOR_BGR2GRAY)
# Binary Threshold
ret, sep_thresh = cv2.threshold(gray_sep_coins, 160, 255, cv2.THRESH_BINARY_INV)
plt.imshow(sep_thresh, 'gray')
# Find Contours
contours, hierarchy = cv2.findContours(sep_thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
for i in range(len(contours)):
    if hierarchy[0][i][3] == -1:
        cv2.drawContours(sep_coins, contours, i, (255, 0, 0), 10)
plt.imshow(sep_coins)

# OpenCV Watershed Algorithm #
sep_coins = cv2.imread(PATH + '/DATA/pennies.jpg')
sep_blur = cv2.medianBlur(sep_coins, 55)
gray_sep_coins = cv2.cvtColor(sep_blur, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(gray_sep_coins, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
# Noise Removal - Important in other examples, effect as cv2.THRESH_OTSU
kernel = np.ones((3, 3), np.uint8)
opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
plt.imshow(opening)
# Distance Transform
dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
ret, sure_fg = cv2.threshold(dist_transform, 0.7*dist_transform.max(), 255, 0)
sure_bg = cv2.dilate(opening, kernel, iterations=3)
sure_fg = np.uint8(sure_fg)
unknown_region = cv2.subtract(sure_bg, sure_fg)
# Label markers
ret, markers = cv2.connectedComponents(sure_fg)
# Transform unknown regions to black
markers = markers + 1
markers[unknown_region == 255] = 0
markers = cv2.watershed(sep_coins, markers)
plt.imshow(markers)
# Count with contours
contours, hierarchy = cv2.findContours(markers, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
for i in range(len(contours)):
    if hierarchy[0][i][3] == -1:
        cv2.drawContours(sep_coins, contours, i, (255, 0, 0), 10)
plt.imshow(sep_coins)

