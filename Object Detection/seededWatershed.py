import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath
from matplotlib import cm


def create_rgb(value):
    return tuple(np.array(cm.tab10(value)[:3]) * 255)


def on_mouse(event, x, y, flags, params):
    global marks_updated
    if event == cv2.EVENT_LBUTTONDOWN:
        # Markers passed to the watershed alg
        cv2.circle(marker_image, (x, y), 5, current_marker, -1)
        # User view
        cv2.circle(road_copy, (x, y), 5, colors[current_marker], -1)
        marks_updated = True


PATH = includePath()

road = cv2.imread(PATH + '/DATA/road_image.jpg')
road_copy = road.copy()
marker_image = np.zeros(road.shape[:2], np.int32)
segments = np.zeros(road.shape, np.uint8)

# Using matplot color map values (R, G, B)

colors = []
for i in range(10):
    colors.append(create_rgb(i))

n_markers = 10
current_marker = 1
marks_updated = False

cv2.namedWindow('Road Image')
cv2.setMouseCallback('Road Image', on_mouse)
while True:
    cv2.imshow('Watershed Segments', segments)
    cv2.imshow('Road Image', road_copy)

    # Break on Esc
    k = cv2.waitKey(1)
    if k == 27:
        break
    # Clear colors on c
    elif k == ord('c'):
        road_copy = road.copy()
        marker_image = np.zeros(road.shape[:2], np.int32)
        segments = np.zeros(road.shape, np.uint8)

    # Update color choice
    elif k > 0 and chr(k).isdigit():
        current_marker = int(chr(k))    # Transforms input to value of color

    # Update the markings
    if marks_updated:
        marker_image_copy = marker_image.copy()
        cv2.watershed(road, marker_image_copy)
        segments = np.zeros(road.shape, np.uint8)
        for color_id in range(n_markers):
            segments[marker_image_copy == color_id] = colors[color_id]  # where the value of marker is certain color,
                                                                        # segments get painted
cv2.destroyAllWindows()
