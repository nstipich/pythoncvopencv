import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

img = cv2.imread(PATH + '/DATA/internal_external.png', 0)
plt.imshow(img, 'gray')

contours, hierarchy = cv2.findContours(img, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
external_contours = np.zeros(img.shape)
internal_contours = np.zeros(img.shape)

for i in range(len(contours)):
    if hierarchy[0][i][3] == -1:    # different groups 0, 1, 4 etc
        cv2.drawContours(external_contours, contours, i, 255, -1)
    else:
        cv2.drawContours(internal_contours, contours, i, 255, -1)

plt.imshow(external_contours)
plt.imshow(internal_contours)
