import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

img = cv2.imread(PATH + '/DATA/sammy_face.jpg')
edges = cv2.Canny(img, threshold1=127, threshold2=255)
plt.imshow(edges)
med_val = np.median(img)
lower = int(max(0, 0.7*med_val))    # either 0 ir 70% of median value
upper = int(min(255, 1.3 * med_val))    # either 130% of median value or 255

blurred_img = cv2.blur(img, (5, 5))

edges = cv2.Canny(blurred_img, threshold1=lower, threshold2=upper + 75)
plt.imshow(edges)
