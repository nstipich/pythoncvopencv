import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath


def detect_face(img):
    face_img = img.copy()
    face_rects = face_cascade.detectMultiScale(face_img)
    for (x, y, w, h) in face_rects:
        cv2.rectangle(face_img, (x, y), (x+w, y+h), (255, 255, 255), 10)
    return face_img


def detect_face_adjusted(img):
    face_img = img.copy()
    face_rects = face_cascade.detectMultiScale(face_img, 1.2, 5)
    for (x, y, w, h) in face_rects:
        cv2.rectangle(face_img, (x, y), (x+w, y+h), (255, 255, 255), 10)
    return face_img


def detect_eyes(img):
    face_img = img.copy()
    eyes_rects = eye_cascade.detectMultiScale(face_img, 1.2, 5)
    for (x, y, w, h) in eyes_rects:
        cv2.rectangle(face_img, (x, y), (x+w, y+h), (255, 255, 255), 10)
    return face_img


PATH = includePath()

nadia = cv2.imread(PATH + '/DATA/Nadia_Murad.jpg', 0)
denis = cv2.imread(PATH + '/DATA/Denis_Mukwege.jpg', 0)
solvay = cv2.imread(PATH + '/DATA/solvay_conference.jpg', 0)
cv2.imread('../DATA/haarcascades/haarcascade_frontalface_default.xml')

# Face Detection
face_cascade = cv2.CascadeClassifier(PATH + '/DATA/haarcascades/haarcascade_frontalface_default.xml')
result_denis = detect_face(denis)
result_nadia = detect_face(nadia)
result_solvay = detect_face_adjusted(solvay)
plt.imshow(result_solvay, 'gray')

# Eyes Detection
eye_cascade = cv2.CascadeClassifier(PATH + '/DATA/haarcascades/haarcascade_eye.xml')
result_nadia = detect_eyes(nadia)
result_denis = detect_eyes(denis)   # edited picture/doesn't work
result_solvay = detect_eyes(solvay)
plt.imshow(result_nadia, 'gray')

# Video detection
cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
while True:
    ret, frame = cap.read(0)
    frame = detect_face(frame)
    cv2.imshow('Video face detector', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
