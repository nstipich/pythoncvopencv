import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

reeses = cv2.imread(PATH + '/DATA/reeses_puffs.png', 0)
cereals = cv2.imread(PATH + '/DATA/many_cereals.jpg', 0)

# orb brute feature matching
orb = cv2.ORB_create()
kp1, des1 = orb.detectAndCompute(reeses, None)
kp2, des2 = orb.detectAndCompute(cereals, None)
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
matches = bf.match(des1, des2)
matches = sorted(matches, key=lambda x:x.distance)
reeses_matches = cv2.drawMatches(reeses, kp1, cereals, kp2, matches[:25], None, flags=2)
cv2.imshow('Matching', reeses_matches)

# SIFT feature matching
sift = cv2.xfeatures2d.SIFT_create()
kp1, des1 = sift.detectAndCompute(reeses, None)
kp2, des2 = sift.detectAndCompute(cereals, None)
bf = cv2.BFMatcher()
matches = bf. knnMatch(des1, des2, 2)
good_matches = []
ratio = 0.7     # ratio test, distance evaluates matches
for match1, match2 in matches:
    if match1.distance < ratio*match2.distance:
        good_matches.append([match1])
len(good_matches)
sift_matches = cv2.drawMatchesKnn(reeses, kp1, cereals, kp2, good_matches, None, flags=2)
cv2.imshow('Sift matches', sift_matches)

# FLANN

sift = cv2.xfeatures2d.SIFT_create()
kp1, des1 = sift.detectAndCompute(reeses, None)
kp2, des2 = sift.detectAndCompute(cereals, None)
FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
search_params = dict(checks=50)

flann = cv2.FlannBasedMatcher(index_params, search_params)
matches = flann.knnMatch(des1, des2, k=2)
matches_mask = [[0, 0] for i in range(len(matches))]
# good_matches = []
for i, (match1, match2) in enumerate(matches):
    if match1.distance < ratio*match2.distance:
        matches_mask[i] = [1, 0]    # 1 for a good match
        # good_matches.append([match1])

draw_params = dict(matchColor=(0, 255, 0), singlePointColor=(255, 0, 0),
                   matchesMask=matches_mask, flags=0)

#flann_matches = cv2.drawMatchesKnn(reeses, kp1, cereals,
#                                   kp2, good_matches, None, flags=0)
flann_matches = cv2.drawMatchesKnn(reeses, kp1, cereals,
                                   kp2, matches, None, **draw_params)   # ** value of dictionary
cv2.imshow('Flann matches', flann_matches)
