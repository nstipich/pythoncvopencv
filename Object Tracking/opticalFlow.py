import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

corner_track_params = dict(maxCorners=30, qualityLevel=0.3, minDistance=7, blockSize=7)
lk_params = dict(winSize=(200, 200), maxLevel=2, criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)

ret, prev_frame = cap.read()    # First frame
prev_gray = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)

# Points to track
prev_pts = cv2.goodFeaturesToTrack(prev_gray, mask=None, **corner_track_params)

# Mask for visualizing
mask = np.zeros_like(prev_frame)
# cv2.namedWindow('Tracking', cv2.WINDOW_NORMAL)

while True:
    ret, frame = cap.read()
    curr_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    next_pts, status, err = cv2.calcOpticalFlowPyrLK(prev_gray, curr_gray,
                                                     prev_pts, None, **lk_params)
    good_new = next_pts[status == 1]    # status is set to 1 if flow found
    good_prev = prev_pts[status == 1]

    for i, (new, prev) in enumerate(zip(good_new, good_prev)):  # zip() create iterable of tuples
        x_new, y_new = new.ravel()  # flattens to 1 array
        x_prev, y_prev = prev.ravel()

        mask = cv2.line(mask, (int(x_new), int(y_new)), (int(x_prev), int(y_prev)), (0, 255, 0), 3)
        frame = cv2.circle(frame, (int(x_new), int(y_new)), 8, (0, 0, 255), -1)
    image = cv2.add(frame, mask)
    cv2.imshow('Tracking', image)

    prev_gray = curr_gray.copy()
    prev_pts = good_new.reshape(-1, 1, 2)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
