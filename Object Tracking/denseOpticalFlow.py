import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

# Colors every(dense) part of the image that move right - red, left - blue, down - green, up - purple
# By converting the vectors calculated with Farneback to polar vectors
# we apply them to HSV color format to receive colors depending on magnitude and angle
cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)

ret, frame1 = cap.read()
prev_img = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)

hsv_mask = np.zeros_like(frame1)
hsv_mask[:, :, 1] = 255

while True:
    ret, frame2 = cap.read()

    next_image = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
    flow = cv2.calcOpticalFlowFarneback(prev_img, next_image, None, 0.5, 3, 15, 3, 5, 1.2, 0)

    mag, ang = cv2.cartToPolar(flow[:, :, 0], flow[:, :, 1], angleInDegrees=True)
    hsv_mask[:, :, 0] = ang/2   # Hue div by 2
    hsv_mask[:, :, 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)   # Normalize value

    bgr = cv2.cvtColor(hsv_mask, cv2.COLOR_HSV2BGR)
    cv2.imshow('Frame', bgr)
    prev_img = next_image

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()

