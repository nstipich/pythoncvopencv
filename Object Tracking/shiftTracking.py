import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

cap = cv2.VideoCapture(0)

ret, frame = cap.read()

face_cascade = cv2.CascadeClassifier(PATH + '/DATA/haarcascades/haarcascade_frontalface_default.xml')
face_rects = face_cascade.detectMultiScale(frame, 1.2, 5)
print(face_rects)

face_x, face_y, w, h = face_rects[0]    # first face and only
track_window = (face_x, face_y, w, h)

roi = frame[face_y:face_y+h, face_x:face_x+w]

hsv_roi = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)

roi_hist = cv2.calcHist([hsv_roi], [0], None, [180], [0, 180])
cv2.normalize(roi_hist, roi_hist, 0, 255, cv2.NORM_MINMAX)

termination_criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)    # terminate after 10 iterations or 1 epsilon

while True:
    ret, frame = cap.read()
    if ret:
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        dst = cv2.calcBackProject([hsv], [0], roi_hist, [0, 180], 1)
        ret, track_window = cv2.meanShift(dst, track_window, termination_criteria)

        x, y, w, h = track_window
        image2 = cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 5)
        cv2.imshow('Image', image2)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break
cap.release()
cv2.destroyAllWindows()
