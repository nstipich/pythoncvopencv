import numpy as np
myList = [2, 2, 3]
print(type(myList))
np.array(myList)
myArray = np.array(myList)
np.arange(0, 10, 4)
np.zeros((5, 5), int)
np.ones((5, 5))    # rows cols

np.random.seed(100)
array = np.random.randint(0, 100, 10)   # run seed in same line
array
array2 = np.random.randint(0, 100, 10)
array.argmax()  # position of max
array2.mean()
array.shape
array.reshape((2, 5))   # must be 10 numbers e.g. 2x5
matrix = np.arange(0, 100).reshape(10, 10)
matrix[9, 9]
matrix[:, 1].reshape(10, 1)
matrix[0:3, 0:3]
matrix[0:3, 0:3] = 77
newMatrix = matrix.copy()
newMatrix[0:3, 0:3] = 44
