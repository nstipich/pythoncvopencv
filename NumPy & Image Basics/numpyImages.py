import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from includePath import includePath

PATH = includePath()
picture = Image.open(PATH + '/DATA/00-puppy.jpg')
picture.show()

pictureArray = np.array(picture)
pictureArray.shape
plt.imshow(pictureArray)
pictureRed = pictureArray[:, :, 0]
plt.imshow(pictureRed, cmap='gray')    # matplotlib has weird color maps for imshow

pictureGreen = pictureArray[:, :, 0]
plt.imshow(pictureGreen, cmap='gray')
