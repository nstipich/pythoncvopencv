import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath
import time
PATH = includePath()

capture = cv2.VideoCapture(PATH + '/DATA/hand_move.mp4')

if not capture.isOpened():
    print('Error loading the file.')

while capture.isOpened():
    ret, frame = capture.read()
    if ret:

        # time.sleep(1/30)
        cv2.imshow('Frame', frame)
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break
    else:
        break
capture.release()
cv2.destroyAllWindows()
