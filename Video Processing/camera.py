import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

capture = cv2.VideoCapture(0, cv2.CAP_DSHOW)

width = int(capture.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
frames = int(capture.get(cv2.CAP_PROP_FPS))

writer = cv2.VideoWriter(PATH + '/DATA/camera.avi', cv2.VideoWriter_fourcc(*'MJPG'), 24, (width, height))
while True:
    ret, frame = capture.read()
    writer.write(frame)
    # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow('Frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
capture.release()
cv2.destroyAllWindows()
