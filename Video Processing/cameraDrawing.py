import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()


def draw_rectangle(event, x, y, flags, param):
    global pt1, pt2, bot_right_clicked, top_left_clicked
    if event == cv2.EVENT_LBUTTONDOWN:
        if top_left_clicked and bot_right_clicked:  # reset of rectangle
            pt1 = (0, 0)
            pt2 = (0, 0)
            top_left_clicked = False
            bot_right_clicked = False
        if not top_left_clicked:
            pt1 = (x, y)
            top_left_clicked = True
        elif not bot_right_clicked:
            pt2 = (x, y)
            bot_right_clicked = True


pt1 = (0, 0)
pt2 = (0, 0)
top_left_clicked = False
bot_right_clicked = False

cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
cv2.namedWindow('Frame')
cv2.setMouseCallback('Frame', draw_rectangle)

width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

# x = int(width/2)  # whole num divide
# y = int(height/2)
# rect_width = int(width/4)
# rect_height = int(height/4)
# x_end = x + rect_width
# y_end = y + rect_height

while True:
    ret, frame = cap.read()
    if top_left_clicked:
        cv2.circle(frame, pt1, 5, (255, 0, 0), -1)
    if top_left_clicked and bot_right_clicked:
        cv2.rectangle(frame, pt1, pt2, (0, 0, 255), 4)

    cv2.imshow('Frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
