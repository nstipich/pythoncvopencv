import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

image = cv2.imread(PATH + '/DATA/00-puppy.jpg')     # for Python Console
# image = cv2.imread('../DATA/00-puppy.jpg')    # for Run

image = np.array(image)
cv2.imshow('Puppy', cv2.resize(image, (512, 512)))
imageHLS = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
cv2.imshow('Puppy HLS', cv2.resize(imageHLS, (512, 512)))
imageHSV = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
cv2.imshow('Puppy HSV', cv2.resize(imageHSV, (512, 512)))
cv2.waitKey()
cv2.destroyAllWindows()

