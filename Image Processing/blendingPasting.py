import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

image1 = cv2.imread(PATH + '/DATA/dog_backpack.png')     # for Python Console
image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2RGB)
image2 = cv2.imread(PATH + '/DATA/watermark_no_copy.png')
image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2RGB)

plt.imshow(image1)
plt.imshow(image2)

image1.shape
image2.shape

image1 = cv2.resize(image1, (512, 512))
image2 = cv2.resize(image2, (512, 512))

blended = cv2.addWeighted(image1, alpha=0.8,    # same image size only
                          src2=image2, beta=0.2,
                          gamma=0)
plt.imshow(blended)

image2 = cv2.resize(image2, (600, 600))

x_offset = 100
y_offset = 100
x_end = x_offset + image2.shape[1]  # in numpy x is 2nd
y_end = y_offset + image2.shape[0]
image1[y_offset:y_end, x_offset:x_end] = image2
plt.imshow(image1)
plt.imshow(image2)

# Part two | Masks

image1 = cv2.imread(PATH + '/DATA/dog_backpack.png')     # for Python Console
image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2RGB)
image2 = cv2.imread(PATH + '/DATA/watermark_no_copy.png')
image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2RGB)
image2 = cv2.resize(image2, (512, 512))
image1.shape

x_offset = 934 - 512
y_offset = 1401 - 512

rows, cols, channels = image2.shape

roi = image1[y_offset:1401, x_offset:943]
plt.imshow(roi)
image2gray = cv2.cvtColor(image2, cv2.COLOR_RGB2GRAY)
plt.imshow(image2gray, cmap='gray')
mask_inv = cv2.bitwise_not(image2gray)
plt.imshow(mask_inv, cmap='gray')
white_background = np.full(image2.shape, 255, dtype=np.uint8)
mask_inv.shape
bk = cv2.bitwise_or(white_background, white_background, mask=mask_inv)  # apply to every channel
plt.imshow(bk)
fg = cv2.bitwise_or(image2, image2, mask=mask_inv)
final_roi = cv2.bitwise_or(roi, fg)
plt.imshow(final_roi)
large_image = image1
large_image[y_offset:y_offset+final_roi.shape[0],
            x_offset:x_offset+final_roi.shape[1]] = final_roi
plt.imshow(large_image)
