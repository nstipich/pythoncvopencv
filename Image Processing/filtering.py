import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()


def load_image():
    image = cv2.imread(PATH + '/DATA/bricks.jpg').astype(np.float32) / 255
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    return image


def display_image(image):
    plt.imshow(image)


img = load_image()
display_image(img)

gamma = 1/4     # bellow 1 - brighter, above - darker
result = np.power(img, gamma)
display_image(result)

img = load_image()
font = cv2.FONT_HERSHEY_COMPLEX
cv2.putText(img, 'Bricks', (10, 600), fontFace=font,
            fontScale=10, color=(255, 0, 0), thickness=4)
display_image(img)

kernel = np.ones((5, 5), np.float32) / 25
result = cv2.filter2D(img, -1, kernel)
display_image(result)

result = cv2.blur(img, ksize=(5, 5))
display_image(result)

result = cv2.GaussianBlur(img, (5, 5), 10)
display_image(result)

result = cv2.medianBlur(img, 5)
display_image(result)

image = cv2.imread(PATH + '/DATA/sammy.jpg')
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
display_image(image)

noise_image = cv2.imread(PATH + '/DATA/sammy_noise.jpg')
display_image(noise_image)
median = cv2.medianBlur(noise_image, 5)
plt.figure(2)
display_image(median)

blur = cv2.bilateralFilter(img, 9, 75, 75)
display_image(blur)
