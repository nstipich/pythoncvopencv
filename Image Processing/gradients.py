import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()


def load_image():
    image = cv2.imread(PATH + '/DATA/sudoku.jpg', 0)
    return image


def display_image(image):
    plt.imshow(image, cmap='gray')


image = load_image()
display_image(image)

sobelx = cv2.Sobel(image, cv2.CV_64F, 1, 0, ksize=5)
display_image(sobelx)
sobely = cv2.Sobel(image, cv2.CV_64F, 0, 1, ksize=5)
display_image(sobely)

laplacian = cv2.Laplacian(image, cv2.CV_64F)
display_image(laplacian)
blend = cv2.addWeighted(sobelx, 0.5, sobely, 0.5, 0)
display_image(blend)
kernel = np.ones((4, 4), np.uint8)
gradient = cv2.morphologyEx(blend, cv2.MORPH_GRADIENT, kernel)
display_image(gradient)
