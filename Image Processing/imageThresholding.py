import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()

image = cv2.imread(PATH + '/DATA/rainbow.jpg', 0)
plt.imshow(image, cmap='gray')
ret, thresh1 = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)
plt.imshow(thresh1, cmap='gray')

image2 = cv2.imread(PATH + '/DATA/crossword.jpg', 0)
plt.imshow(image2, cmap='gray')
ret, th1 = cv2.threshold(image2, 180, 255, cv2.THRESH_BINARY)
plt.imshow(th1, cmap='gray')
th2 = cv2.adaptiveThreshold(image2, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,
                            11, 8)
plt.imshow(th2, cmap='gray')
blended = cv2.addWeighted(thresh1, 0.6, th2, 0.4, 0)
plt.imshow(blended, cmap='gray')
