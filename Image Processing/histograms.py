import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()


def display_image(image):
    plt.imshow(image, cmap='gray')


dark_horse = cv2.imread(PATH + '/DATA/horse.jpg')
show_horse = cv2.cvtColor(dark_horse, cv2.COLOR_BGR2RGB)

rainbow = cv2.imread(PATH + '/DATA/rainbow.jpg')
show_rainbow = cv2.cvtColor(rainbow, cv2.COLOR_BGR2RGB)

blue_bricks = cv2.imread(PATH + '/DATA/bricks.jpg')
show_bricks = cv2.cvtColor(blue_bricks, cv2.COLOR_BGR2RGB)

hist_values = cv2.calcHist([blue_bricks], channels=[0], mask=None, histSize=[256],
                            ranges=[0, 256])
plt.plot(hist_values)

hist_values = cv2.calcHist([dark_horse], channels=[0], mask=None, histSize=[256],
                            ranges=[0, 256])
plt.plot(hist_values)

color = ('b', 'g', 'r')
for i, color in enumerate(color):
    hist = cv2.calcHist([blue_bricks], [i], None, [256], [0, 256])
    plt.plot(hist, color=color)
    plt.xlim([0, 256])
    # plt.ylim([0, 500000])
plt.title('histogram for blue bricks')

# Histogram Equalization

rainbow.shape
mask = np.zeros(rainbow.shape[:2], np.uint8)      # 2 is inclusive
plt.imshow(mask, cmap='gray')
mask[300:400, 100:400] = 255
masked_image = cv2.bitwise_and(rainbow, rainbow, mask=mask)
show_masked_image = cv2.bitwise_and(show_rainbow, show_rainbow, mask=mask)
plt.imshow(show_masked_image)
hist_mask_values_red = cv2.calcHist([rainbow], [2], mask, [256], [0, 256])
hist_values_red = cv2.calcHist([rainbow], [2], mask=None, histSize=[256], ranges=[0, 256])
plt.plot(hist_mask_values_red)
plt.plot(hist_values_red)

gorilla = cv2.imread(PATH + '/DATA/gorilla.jpg', 0)
show_gorilla = cv2.imread(PATH + '/DATA/gorilla.jpg')
show_gorilla = cv2.cvtColor(show_gorilla, cv2.COLOR_BGR2RGB)

display_image(gorilla)
hist_values_gorilla = cv2.calcHist([gorilla], [0], None, [256], [0, 256])
plt.plot(hist_values_gorilla)

eq_gorilla = cv2.equalizeHist(gorilla)
display_image(eq_gorilla)
hist_values_eq_gorilla = cv2.calcHist([eq_gorilla], [0], None, [256], [0, 256])
plt.plot(hist_values_eq_gorilla)

display_image(show_gorilla)
hsv_gorilla = cv2.cvtColor(show_gorilla, cv2.COLOR_RGB2HSV)
hsv_gorilla[:, :, 2]    # only value channel

hsv_gorilla[:, :, 2] = cv2.equalizeHist(hsv_gorilla[:, :, 2])

eq_gorilla_color = cv2.cvtColor(hsv_gorilla, cv2.COLOR_HSV2RGB)
display_image(eq_gorilla_color)
