import numpy as np
import matplotlib.pyplot as plt
import cv2
from includePath import includePath

PATH = includePath()


def load_image():
    image = np.zeros((600, 600))
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(image, 'ABCDE', (50, 300), font, 5,
                (255, 255, 255), 25)
    return image


def display_image(image):
    plt.imshow(image, cmap='gray')


image = load_image()
display_image(image)

kernel = np.ones((5, 5), dtype=np.uint8)
result = cv2.erode(image, kernel, iterations=4)
display_image(result)

white_noise = np.random.randint(0, 2, (600, 600)) * 255   # 0-1 possible
display_image(white_noise)
noise_image = image + white_noise
display_image(noise_image)

opening = cv2.morphologyEx(noise_image, cv2.MORPH_OPEN, kernel)
display_image(opening)      # cleaning bg noise

black_noise = np.random.randint(0, 2, (600, 600)) * -255
display_image(black_noise)
black_noise_image = image + black_noise
black_noise_image[black_noise_image == -255] = 0
display_image(black_noise_image)

closing = cv2.morphologyEx(black_noise_image, cv2.MORPH_CLOSE, kernel)
display_image(closing)

gradient = cv2.morphologyEx(image, cv2.MORPH_GRADIENT, kernel)
display_image(gradient)

